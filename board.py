from random import randint

class Board(object):

    # default conf file
    conf_file = 'default.conf'

    def __init__(self, conf=None):
        # check if conf is given
        if not conf:
            self.conf = self.load_conf_(self.conf_file)

    # set new config
    def set(self,conf):
        self.conf = conf

    # reset to initial config
    def reset(self):
        self.conf = self.load_conf_(conf_file)

    # load conf from file
    def load_conf_(self, conf_file):
        with open(conf_file) as f:
            content = f.read()
        return [ line.split(',') for line in content[:-1].split('\n') ]

    # set conf to self.conf
    def load_conf(self,conf_file):
        self.conf = self.load_conf_(conf_file)

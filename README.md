# Chess Template

GUI + basic utilities for building a chess game in python. 

![](/img/screenshot.png)


## Why?

Learning to express a logic in a programming language, is the best way to understand the constructs and idioms of a language. The objective of this project is to provide a few abstractions, over which the students can build their logic and witness the results. 

## Modules

- [x] Tkinter based Graphics Rendering
- [x] Board configuration to GUI **adapter**
- [x] Template for Board object
- [ ] Highlight possible locations
- [ ] Support human-computer game (Single Player)

## Setup

```bash
# install dependencies
```

from types import MethodType
from random import randint

from board import Board
from gui import display

def random_logic(self, turn):
    pieces = list(set( [ item for slist in self.conf for item in slist ]))
    ri = lambda : randint(0,7)
    rp = lambda : randint(0,len(pieces)-1)
    x,y,p = ri(),ri(),rp()
    print(x,y,p)
    self.conf[x][y] = pieces[p]

def logic(self, turn):
    # turn : (True->White's turn), (False->Black's turn)
    if turn:
        print("White's turn") 
    else: 
        print("Black's turn")
    # get configuration
    conf = self.conf
    #print(conf)
    # YOUR LOGIC GOES HERE
    conf[1][3] = '0'
    conf[3][3] = 'blackp'
    # this logic will move the pawn at (1,3) to (3,3)

if __name__ == '__main__':
    # create a new board
    my_board = Board()
    # add logic to our board
    #my_board.next_move = MethodType(logic,my_board)
    my_board.next_move = MethodType(logic,my_board)
    # start GUI
    display(my_board)
